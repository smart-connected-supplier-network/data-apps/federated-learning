# Federated Learning Data App

This respository contains the logic to orchestrate Federated Learning with IDS communication. The code in this repository acts as a gateway between the [Federated Learning Helper](https://gitlab.com/tno-tsg/helpers/federated-learning-helper) and the [TSG Core Container](https://gitlab.com/tno-tsg/core-container).

The Federated Learning Data App is built using the [TSG Base Data App library](https://gitlab.com/tno-tsg/data-apps/base-data-app), and can be used in three different modes: The `RESEARCHER` mode, the `TRUSTED_COMPUTE_NODE` mode, and the `WORKER` mode. The `RESEARCHER` mode is typically used by one connector in the data space, while the `WORKER` mode is typically used by multiple connectors. A typical setup for Federated Learning is showed in the picture below.

![Architecture](architecture.drawio.svg)

There is also an option to move the researcher to a separate connector and use a `TRUSTED_COMPUTE_NODE`. This node can handle the averaging of the weights, to prevent the researcher from gaining knowledge about the data through the weights of the models. In a more default setup, the `TRUSTED_COMPUTE_NODE` is the same as the researcher connector.

## Deployment
The way to deploy this Data App, is via the [TSG Connector Helm Chart](https://gitlab.com/tno-tsg/helm-charts/connector), providing the following configuration for a worker:

```yaml
containers:  
  - type: data-app
    image: docker.nexus.dataspac.es/federated-learning/data-app:1.0.0 # image of this repository
    apiKey: APIKEY-Example123 # APIKEY as given by the TSG
    name: federated-learning
    config:
      fl:
        type: WORKER # Pick one for each TSG
        backend: http://{{ template "tsg-connector.fullname" . }}-tf-backend-http:8080 # Name of the Python backend
        trustedComputeNode: urn:ids:connectors:Researcher # IDS ID of the Trusted Compute Node
        pull: false # 
```
This configuration can be used for the `TRUSTED_COMPUTE_NODE` with an embedded researcher:
```yaml
containers:  
  - type: data-app
    image: docker.nexus.dataspac.es/federated-learning/data-app:1.0.0 # image of this repository
    apiKey: APIKEY-Example456 # APIKEY as given by the TSG
    name: federated-learning
    config:
      fl:
          type: TRUSTED_COMPUTE_NODE
          backend: http://{{ template "tsg-connector.fullname" . }}-tf-backend-http:8080
          trustedComputeNode: urn:ids:connectors:Researcher # own IDS ID
          pull: false
          embeddedResearcher: true
```
## Endpoints
The Federated Learning Data App contains the following endpoints:

### Researcher
| Endpoint | Method | Request | Response | Description |
|---|---|---|---|---|
| `/connectors` | `GET` | - | ["urn:ids:connectors:ID"] | Retrieve the connectors in the dataspace via the broker. |
| `/connectors/datasets` | `GET` | - | [{"datasetName": "Test Data Set" "connectors": ["urn:ids:connectors:ID"]}] | Retrieve the data sets and the corresponding connectors in the dataspace via the broker. |
| `initialize` | `POST` | {"title": string,"rounds": int,"epochs": int,"batch_size": int ,"validation_split": float ,"normalization": string,"shape":[int],"label_column": string,"loss": string,"metrics":[string],"model": {JSON structure of Keras model},"optimizer":{JSON structure of Keras Optimizer},"workers":[string],"key": string} | - | Initialize the Federated Learning process with some necessary parameters. |
| `train` | `POST` | - | - | Start the training of the model. |
| `status` | `GET` | - | List of metrics, as given in the 'initialize' call e.g. [[{"loss": float,"val_loss": float,"root_mean_squared_error": float,"round": int,"epoch": int,"worker": string,"val_root_mean_squared_error":float ,"timestamp": int}]] | Get the status of the federated learning, a list of results is published, each connectors produces its own list. The lists per connector contain one entry per epoch. |
| `model` | `GET` | - | Full model in h5 format | Download the model in h5 format. |

### Worker
| Endpoint | Method | Request | Response | Description |
|---|---|---|---|---|
| `/status` | `POST` | {'loss': float, 'root_mean_squared_error': float, 'val_loss': float, 'val_root_mean_squared_error': float, 'epoch': int, round: int} | - | Endpoint to share the metrics of the federated learning epoch with the researcher. |
| `/model` | `POST` | Trained model | - | Send the trained model to the researcher |

### Dataset Handler
| Endpoint | Method | Request | Response | Description |
|---|---|---|---|---|
| `/datasets/{key}` | `POST` | {key: string, dataset: string} | - | Stores the dataset on the filesystem of the container. |
| `/datasets/{key}` | `DELETE` | key: string | - | Delete the file from the filesystem of the container. |

## Build

If you do not want to use the provided Docker image (`docker.nexus.dataspac.es/data-apps/federated-learning:1.0.0`), you can build the OpenAPI data app yourself using Gradle:
```shell
./gradlew clean assemble
```
Some tests are implemented to verify basic functionality of the data-app:
```shell
./gradlew test
```
Via gradle Docker images can be created automatically, for both amd64 and arm64, without requirement of Docker daemon:
```shell
./gradlew jib
```
By executing this, the images will be directly pushed to the corresponding Docker registry.
If you opt for building the image just for local usage, using the Docker daemon is also possible:
```shell
./gradlew jibDockerBuild
```


## Configuration

All the configuration that is possible for the Federated Learning data app is displayed in the table below.

| <div style="width: 400px">Key</div> | <div style="width: 50px">Type</div> | <div style="width: 300px">Default</div> | Description |
|-----|------|---------|-------------|
| fl.type | `WORKER`, `TRUSTED_COMPUTE_NODE`, `RESEARCHER` | `nil` | Deploy the instance as researcher, trusted compute node or worker. |
| fl.backend | string | `nil` | Tensorflow backend URL. |
| fl.trustedComputeNode | string | `nil` | URL of the Trusted Compute node. |
| fl.pull | bool | `true` | Whether there should be polling |
| fl.longPollingInterval | int | 20 | Polling interval in seconds. |
| fl.embeddedResearcher | bool | `false` | DEPRECATED: If the researcher should be inbedded or not | 