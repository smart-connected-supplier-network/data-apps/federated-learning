package nl.tno.ids.fl.worker

import de.fraunhofer.iais.eis.*
import nl.tno.ids.base.BrokerClient
import nl.tno.ids.base.HttpHelper
import nl.tno.ids.base.MessageHandler
import nl.tno.ids.base.ResponseBuilder
import nl.tno.ids.base.configuration.IdsConfig
import nl.tno.ids.common.serialization.DateUtil
import nl.tno.ids.fl.FederatedLearningConfig
import nl.tno.ids.fl.trusted.InvokeOperationMessageHandler
import org.apache.commons.codec.binary.Base64InputStream
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.InputStreamEntity
import org.apache.http.impl.client.CloseableHttpClient
import org.json.JSONObject
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler
import org.springframework.stereotype.Component
import java.io.InputStream
import java.net.URI
import java.time.Duration
import java.time.Instant
import java.util.concurrent.ConcurrentHashMap

@Component
@ConditionalOnProperty("fl.type", havingValue = "WORKER")
class InvokeOperationMessageHandler(
    private val responseBuilder: ResponseBuilder,
    private val federatedLearningConfig: FederatedLearningConfig,
    private val httpClient: CloseableHttpClient
) : MessageHandler<InvokeOperationMessage> {

    companion object {
        private val LOG = LoggerFactory.getLogger(InvokeOperationMessageHandler::class.java)
    }

    override fun handle(header: InvokeOperationMessage, payload: InputStream?, httpHeaders: HttpHeaders): ResponseEntity<*> {
        LOG.info("Handling pulled message ${header.operationReference} (${payload?.available()} bytes)")
        return when (header.operationReference.toString()) {
            "urn:ids:fl:initialize" -> {
                val httpPost = HttpPost("${federatedLearningConfig.backend}/initialize")
                httpPost.entity = InputStreamEntity(payload)
                httpClient.execute(httpPost).use { response ->
                    if (response.statusLine.statusCode !in 200..299) {
                        LOG.warn("Can't initialize backend service: ${response.statusLine.statusCode}")
                        responseBuilder.createRejectionResponse(header, RejectionReason.INTERNAL_RECIPIENT_ERROR, HttpStatus.valueOf(response.statusLine.statusCode))
                    } else {
                        responseBuilder.createMessageProcessedResponse(header)
                    }
                }
            }
            "urn:ids:fl:model" -> {
                val httpPost = HttpPost("${federatedLearningConfig.backend}/model")
                httpPost.entity = InputStreamEntity(Base64InputStream(payload))
                httpClient.execute(httpPost).use { response ->
                    if (response.statusLine.statusCode !in 200..299) {
                        LOG.warn("Can't upload model to backend service: ${response.statusLine.statusCode}")
                        responseBuilder.createRejectionResponse(header, RejectionReason.INTERNAL_RECIPIENT_ERROR, HttpStatus.valueOf(response.statusLine.statusCode))
                    } else {
                        responseBuilder.createMessageProcessedResponse(header)
                    }
                }
            }
            "urn:ids:fl:finish" -> {
                val httpPost = HttpPost("${federatedLearningConfig.backend}/finish")
                httpClient.execute(httpPost).use { response ->
                    if (response.statusLine.statusCode !in 200..299) {
                        LOG.warn("Can't send finish signal to backend service: ${response.statusLine.statusCode}")
                        responseBuilder.createRejectionResponse(header, RejectionReason.INTERNAL_RECIPIENT_ERROR, HttpStatus.valueOf(response.statusLine.statusCode))
                    } else {
                        responseBuilder.createMessageProcessedResponse(header)
                    }
                }
            }
            else -> responseBuilder.createRejectionResponse(header, RejectionReason.METHOD_NOT_SUPPORTED, HttpStatus.METHOD_NOT_ALLOWED)
        }
    }
}

@Component
@ConditionalOnProperty("fl.type", havingValue = "WORKER")
class WorkerState(
    private val federatedLearningConfig: FederatedLearningConfig,
    private val brokerClient: BrokerClient,
    private val idsConfig: IdsConfig,
    private val httpHelper: HttpHelper,
    private val handler: nl.tno.ids.fl.worker.InvokeOperationMessageHandler
) {
    companion object {
        private val LOG = LoggerFactory.getLogger(WorkerState::class.java)
    }

    private val runnable = Runnable {
        while(true) {
            try {
                brokerClient.getConnector(federatedLearningConfig.trustedComputeNode!!)?.let { connector ->
                    val accessUrl = connector.hasDefaultEndpoint.accessURL?.toString()
                        ?: throw Exception("No access URL found for connector ${federatedLearningConfig.trustedComputeNode}")
                    val response = httpHelper.toHTTP(accessUrl, createQueryMessage(), "")
                    when (val responseHeader = response.second.header) {
                        is RejectionMessage -> {
                            when (responseHeader.rejectionReason) {
                                RejectionReason.NOT_FOUND -> LOG.debug("No content")
                                else -> LOG.warn("Unexpected rejection: ${responseHeader.rejectionReason}")
                            }
                        }
                        is InvokeOperationMessage -> {
                            val httpHeaders = HttpHeaders().apply {
                                response.second.httpHeaders.forEach { (key, value) ->
                                    add(key, value)
                                }
                            }
                            handler.handle(
                                responseHeader,
                                response.second.payload?.inputStream(),
                                httpHeaders
                            )
                        }
                        else -> LOG.warn("Unexpected Header type: ${responseHeader::class.simpleName}")
                    }
                } ?: LOG.warn("Connector with id ${federatedLearningConfig.trustedComputeNode} not found")
            } catch (e: Exception) {
                LOG.warn("Exception in long-polling thread:",e)
            }
        }
    }

    init {
        assert(federatedLearningConfig.trustedComputeNode !== null)
        if (federatedLearningConfig.pull) {
            Thread(runnable).start()
        }
    }

    private fun createQueryMessage() = QueryMessageBuilder()
        ._modelVersion_("4.1.1")
        ._securityToken_(
            DynamicAttributeTokenBuilder()
                ._tokenFormat_(TokenFormat.JWT)
                ._tokenValue_("DUMMY")
                .build()
        )
        ._issued_(DateUtil.now())
        ._issuerConnector_(URI.create(idsConfig.connectorId))
        ._senderAgent_(URI.create(idsConfig.participantId))
        ._recipientAgent_(URI.create(federatedLearningConfig.trustedComputeNode))
        ._recipientConnector_(URI.create(federatedLearningConfig.trustedComputeNode))
        .build()
}