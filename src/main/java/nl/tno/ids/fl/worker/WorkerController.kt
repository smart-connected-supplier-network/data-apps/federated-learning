package nl.tno.ids.fl.worker

import de.fraunhofer.iais.eis.DynamicAttributeTokenBuilder
import de.fraunhofer.iais.eis.InvokeOperationMessageBuilder
import de.fraunhofer.iais.eis.RejectionMessage
import de.fraunhofer.iais.eis.TokenFormat
import nl.tno.ids.base.BrokerClient
import nl.tno.ids.base.HttpHelper
import nl.tno.ids.base.configuration.IdsConfig
import nl.tno.ids.common.serialization.DateUtil
import nl.tno.ids.fl.FederatedLearningConfig
import org.apache.commons.codec.binary.Base64InputStream
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.io.InputStream
import java.net.URI
import java.util.concurrent.CompletableFuture

@RestController
@ConditionalOnProperty("fl.type", havingValue = "WORKER")
class WorkerController(
    private val brokerClient: BrokerClient,
    private val httpHelper: HttpHelper,
    private val idsConfig: IdsConfig,
    private val federatedLearningConfig: FederatedLearningConfig
) {
    companion object {
        private val LOG = LoggerFactory.getLogger(WorkerController::class.java)
    }

    @PostMapping("status")
    fun status(@RequestBody body: String): CompletableFuture<ResponseEntity<String>> {
        LOG.info("Received status from backend")
        return CompletableFuture.supplyAsync {
            brokerClient.getConnector(federatedLearningConfig.trustedComputeNode!!)?.let { connector ->
                val accessUrl = connector.hasDefaultEndpoint.accessURL?.toString() ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "No access URL found for connector ${federatedLearningConfig.trustedComputeNode}")
                val header = InvokeOperationMessageBuilder()
                    ._modelVersion_("4.1.1")
                    ._securityToken_(
                        DynamicAttributeTokenBuilder()
                            ._tokenFormat_(TokenFormat.JWT)
                            ._tokenValue_("DUMMY")
                            .build()
                    )
                    ._operationReference_(URI.create("urn:ids:fl:trainstatus"))
                    ._issued_(DateUtil.now())
                    ._issuerConnector_(URI.create(idsConfig.connectorId))
                    ._senderAgent_(URI.create(idsConfig.participantId))
                    ._recipientAgent_(URI.create(federatedLearningConfig.trustedComputeNode))
                    ._recipientConnector_(URI.create(federatedLearningConfig.trustedComputeNode))
                    .build()
                val response = httpHelper.toHTTP(accessUrl, header, body)
                when (response.second.header) {
                    is RejectionMessage -> ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("")
                    else -> ResponseEntity.ok("")
                }
            } ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Connector with id ${federatedLearningConfig.trustedComputeNode} not found")
        }
    }

    @PostMapping("model")
    fun model(
        inputStream: InputStream
    ): CompletableFuture<ResponseEntity<String>> {
        LOG.info("Received trained model from backend")
        return CompletableFuture.supplyAsync {
            brokerClient.getConnector(federatedLearningConfig.trustedComputeNode!!)?.let { connector ->
                val accessUrl = connector.hasDefaultEndpoint.accessURL?.toString() ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "No access URL found for connector ${federatedLearningConfig.trustedComputeNode}")
                val header = InvokeOperationMessageBuilder()
                    ._modelVersion_("4.1.1")
                    ._securityToken_(
                        DynamicAttributeTokenBuilder()
                            ._tokenFormat_(TokenFormat.JWT)
                            ._tokenValue_("DUMMY")
                            .build()
                    )
                    ._operationReference_(URI.create("urn:ids:fl:model"))
                    ._issued_(DateUtil.now())
                    ._issuerConnector_(URI.create(idsConfig.connectorId))
                    ._senderAgent_(URI.create(idsConfig.participantId))
                    ._recipientAgent_(URI.create(federatedLearningConfig.trustedComputeNode))
                    ._recipientConnector_(URI.create(federatedLearningConfig.trustedComputeNode))
                    .build()
                val response = httpHelper.toHTTP(accessUrl, header, Base64InputStream(inputStream, true))
                when (response.second.header) {
                    is RejectionMessage -> ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("")
                    else -> ResponseEntity.ok("")
                }
            } ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Connector with id ${federatedLearningConfig.trustedComputeNode} not found")
        }
    }
}