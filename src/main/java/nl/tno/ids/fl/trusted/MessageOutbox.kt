package nl.tno.ids.fl.trusted

import de.fraunhofer.iais.eis.Message
import nl.tno.ids.base.BrokerClient
import nl.tno.ids.base.HttpHelper
import nl.tno.ids.fl.FederatedLearningConfig
import org.apache.http.entity.ContentType
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException
import java.nio.file.Path
import java.util.concurrent.ConcurrentHashMap
import kotlin.io.path.inputStream
import kotlin.io.path.name

data class OutboxMessage(val idsid: String, val header: Message, val payload: String?, val path: Path? = null, val contentType: ContentType)

@Component
@ConditionalOnProperty("fl.type", havingValue = "TRUSTED_COMPUTE_NODE")
class MessageOutbox(
    private val federatedLearningConfig: FederatedLearningConfig,
    private val httpHelper: HttpHelper,
    private val brokerClient: BrokerClient
) {
    companion object {
        private val LOG = LoggerFactory.getLogger(MessageOutbox::class.java)
    }
    val outbox: MutableMap<String, MutableList<OutboxMessage>> = ConcurrentHashMap()

    fun add(idsid: String, header: Message, payload: String? = null, path: Path? = null, contentType: ContentType = ContentType.APPLICATION_JSON) {
        if (federatedLearningConfig.pull) {
            LOG.info("Storing outboxmessage (${payload?.length} bytes / ${path?.name})")
            val outboxMessage = OutboxMessage(idsid, header, payload, path, contentType)
            outbox.getOrPut(idsid) { mutableListOf() }.add(outboxMessage)
        } else {
            brokerClient.getConnector(idsid)?.let { connector ->
                val accessUrl = connector.hasDefaultEndpoint.accessURL?.toString() ?: throw ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "No access URL found for connector ${federatedLearningConfig.trustedComputeNode}")
                if (path != null) {
                    httpHelper.toHTTP(accessUrl, header, path.inputStream(), contentType)
                } else {
                    httpHelper.toHTTP(accessUrl, header, payload, contentType)
                }
            }
        }
    }
}