/*
 * Copyright 2016 Fraunhofer-Institut für Materialfluss und Logistik IML und Fraunhofer-Institut für Software- und Systemtechnik ISST
 * Industrial Data Space, Reference Use Case Logistics
 *
 * For license information or if you have any questions contact us at ids-ap2@isst.fraunhofer.de.
 */
package nl.tno.ids.fl

import de.fraunhofer.iais.eis.ids.jsonld.Serializer
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

/**
 * This is a Spring Boot Application.
 *
 * @author [Maarten Kollenstart](mailto:maarten.kollenstart@tno.nl)
 * @see [Spring Boot](http://projects.spring.io/spring-boot/)
 */
@SpringBootApplication(
    exclude = [DataSourceAutoConfiguration::class, DataSourceTransactionManagerAutoConfiguration::class, HibernateJpaAutoConfiguration::class, AopAutoConfiguration::class],
    scanBasePackages = ["nl.tno.ids"]
)
@ConfigurationPropertiesScan("nl.tno.ids")
class Application

fun main(args: Array<String>) {
    Serializer.addKnownNamespace("nlaic", "https://nlaic.com/")
    runApplication<Application>(*args)
}
