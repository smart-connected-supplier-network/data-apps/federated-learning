package nl.tno.ids.fl

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.lang.Nullable

@ConstructorBinding
@ConfigurationProperties(prefix = "fl")
data class FederatedLearningConfig(
    val type: FederatedLearningType,
    @Nullable val backend: String? = null,
    @Nullable val trustedComputeNode: String? = null,
    val pull: Boolean = true,
    val longPollingInterval: Int = 20,
    val embeddedResearcher: Boolean = false
)

enum class FederatedLearningType {
    WORKER,
    TRUSTED_COMPUTE_NODE,
    RESEARCHER
}
