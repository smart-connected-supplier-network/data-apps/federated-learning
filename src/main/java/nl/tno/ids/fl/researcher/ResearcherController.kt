package nl.tno.ids.fl.researcher

import de.fraunhofer.iais.eis.InvokeOperationMessageBuilder
import de.fraunhofer.iais.eis.RejectionMessage
import nl.tno.ids.base.BrokerClient
import nl.tno.ids.base.HttpHelper
import nl.tno.ids.base.configuration.IdsConfig
import nl.tno.ids.base.infomodel.defaults
import nl.tno.ids.fl.FederatedLearningConfig
import org.apache.commons.codec.binary.Base64InputStream
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.core.io.ByteArrayResource
import org.springframework.core.io.InputStreamResource
import org.springframework.core.io.Resource
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.server.ResponseStatusException
import java.io.File
import java.net.URI
import java.util.concurrent.CompletableFuture

data class DatasetWithConnectors(val datasetName: String, val connectors: List<String>)

@RestController
@ConditionalOnProperty("fl.type", havingValue = "RESEARCHER")
class ResearcherController(
    private val brokerClient: BrokerClient,
    private val httpHelper: HttpHelper,
    private val idsConfig: IdsConfig,
    private val federatedLearningConfig: FederatedLearningConfig
) {
    companion object {
        private val LOG = LoggerFactory.getLogger(ResearcherController::class.java)
    }

    @GetMapping("connectors")
    fun listConnectors(): List<String> {
        LOG.info("Received connectors query")
        return brokerClient.updateConnectors().map { it.key }
    }

    @GetMapping("connectors/datasets")
    fun listConnectorsWithDatasets(): List<DatasetWithConnectors> {
        LOG.info("Received datasets query")
        val connectors = brokerClient.updateConnectors()

        return connectors.flatMap { (connectorId, connector) ->
            connector.resourceCatalog?.flatMap {
                it?.offeredResource?.map { offer ->
                    offer.title.first().value to connectorId
                } ?: emptyList()
            } ?: emptyList()
        }.groupBy { pair -> pair.first }.map { group ->
            DatasetWithConnectors(group.key, group.value.map { it.second })
        }
    }

    @PostMapping("initialize")
    fun initialize(@RequestBody body: String): CompletableFuture<ResponseEntity<String>> {
        return CompletableFuture.supplyAsync {
            brokerClient.getConnector(federatedLearningConfig.trustedComputeNode!!)?.let { connector ->
                LOG.info("Received initial training parameters")
                val accessUrl = connector.hasDefaultEndpoint.accessURL?.toString() ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "No access URL found for connector ${federatedLearningConfig.trustedComputeNode}")
                val header = InvokeOperationMessageBuilder()
                    ._operationReference_(URI.create("urn:ids:fl:initialize"))
                    ._issuerConnector_(URI.create(idsConfig.connectorId))
                    ._senderAgent_(URI.create(idsConfig.participantId))
                    ._recipientAgent_(URI.create(federatedLearningConfig.trustedComputeNode))
                    ._recipientConnector_(URI.create(federatedLearningConfig.trustedComputeNode))
                    .build()
                    .defaults()
                val response = httpHelper.toHTTP(accessUrl, header, body)
                when (response.second.header) {
                    is RejectionMessage -> ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("")
                    else -> ResponseEntity.ok("")
                }
            } ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Connector with id ${federatedLearningConfig.trustedComputeNode} not found")
        }
    }

    @PostMapping("train")
    fun train(@RequestPart file: MultipartFile): CompletableFuture<ResponseEntity<String>> {
        return CompletableFuture.supplyAsync {
            val tmpFile = File.createTempFile("model", ".tmp")
            Base64InputStream(file.inputStream, true).transferTo(tmpFile.outputStream())
            brokerClient.getConnector(federatedLearningConfig.trustedComputeNode!!)?.let { connector ->
                LOG.info("Received initial training model")
                val accessUrl = connector.hasDefaultEndpoint.accessURL?.toString() ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "No access URL found for connector ${federatedLearningConfig.trustedComputeNode}")
                val header = InvokeOperationMessageBuilder()
                    ._operationReference_(URI.create("urn:ids:fl:train"))
                    ._issuerConnector_(URI.create(idsConfig.connectorId))
                    ._senderAgent_(URI.create(idsConfig.participantId))
                    ._recipientAgent_(URI.create(federatedLearningConfig.trustedComputeNode))
                    ._recipientConnector_(URI.create(federatedLearningConfig.trustedComputeNode))
                    .build()
                    .defaults()
                val response = httpHelper.toHTTP(accessUrl, header, tmpFile.inputStream())
                when (response.second.header) {
                    is RejectionMessage -> ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("")
                    else -> ResponseEntity.ok("")
                }
            } ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Connector with id ${federatedLearningConfig.trustedComputeNode} not found")
        }
    }

    @GetMapping("status", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun status(): CompletableFuture<ResponseEntity<String>> {
        return CompletableFuture.supplyAsync {
            brokerClient.getConnector(federatedLearningConfig.trustedComputeNode!!)?.let { connector ->
                val accessUrl = connector.hasDefaultEndpoint.accessURL?.toString() ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "No access URL found for connector ${federatedLearningConfig.trustedComputeNode}")
                val header = InvokeOperationMessageBuilder()
                    ._operationReference_(URI.create("urn:ids:fl:status"))
                    ._issuerConnector_(URI.create(idsConfig.connectorId))
                    ._senderAgent_(URI.create(idsConfig.participantId))
                    ._recipientAgent_(URI.create(federatedLearningConfig.trustedComputeNode))
                    ._recipientConnector_(URI.create(federatedLearningConfig.trustedComputeNode))
                    .build()
                    .defaults()
                val response = httpHelper.toHTTP(accessUrl, header, "")
                when (val responseHeader = response.second.header) {
                    is RejectionMessage -> ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseHeader.rejectionReason.name)
                    else -> ResponseEntity.ok(response.second.payload?.asString() ?: "")
                }
            } ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Connector with id ${federatedLearningConfig.trustedComputeNode} not found")
        }
    }

    @GetMapping("model")
    fun model(): CompletableFuture<ResponseEntity<Resource>> {
        return CompletableFuture.supplyAsync {
            brokerClient.getConnector(federatedLearningConfig.trustedComputeNode!!)?.let { connector ->
                val accessUrl = connector.hasDefaultEndpoint.accessURL?.toString() ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "No access URL found for connector ${federatedLearningConfig.trustedComputeNode}")
                val header = InvokeOperationMessageBuilder()
                    ._operationReference_(URI.create("urn:ids:fl:getModel"))
                    ._issuerConnector_(URI.create(idsConfig.connectorId))
                    ._senderAgent_(URI.create(idsConfig.participantId))
                    ._recipientAgent_(URI.create(federatedLearningConfig.trustedComputeNode))
                    ._recipientConnector_(URI.create(federatedLearningConfig.trustedComputeNode))
                    .build()
                    .defaults()
                val response = httpHelper.toHTTP(accessUrl, header, "")
                when (val responseHeader = response.second.header) {
                    is RejectionMessage -> ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ByteArrayResource(responseHeader.rejectionReason.name.toByteArray()))
                    else -> ResponseEntity.ok().header("Content-Type", "application/x-hdf5").body(response.second.payload?.let { InputStreamResource(Base64InputStream(it.inputStream())) })
                }
            } ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Connector with id ${federatedLearningConfig.trustedComputeNode} not found")
        }
    }

}